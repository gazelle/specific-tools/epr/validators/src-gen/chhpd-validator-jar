package net.ihe.gazelle.hpd.validator.chcpi;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.*;

import java.util.List;


/**
 * class :        SearchRequestSpec
 * package :   chcpi
 * Constraint Spec Class
 * class of test : SearchRequest
 */
public final class SearchRequestSpecValidator {


    private SearchRequestSpecValidator() {
    }


    /**
     * Validation of instance by a constraint : constraint_ch_cpi_002
     * The searchRequest is supported on the CPI root or one of its child elements: ou=CHCommunity or ou=CHEndpoint.
     */
    private static boolean _validateSearchRequestSpec_Constraint_ch_cpi_002(net.ihe.gazelle.hpd.SearchRequest aClass) {
        return ((aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*ou=chcommunity,dc=cpi.*") || aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*ou=chendpoint,dc=cpi.*")) || aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*dc=cpi.*"));

    }

    /**
     * Validation of instance by a constraint : constraint_ch_cpi_004
     * The CPI base distinguished name / directory root is dc=CPI,o=BAG,c=CH.
     */
    private static boolean _validateSearchRequestSpec_Constraint_ch_cpi_004(net.ihe.gazelle.hpd.SearchRequest aClass) {
        return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*,dc=cpi,o=bag,c=ch");

    }

    /**
     * Validation of instance by a constraint : constraint_ch_cpi_006
     * Each community of object class “CHCommunity” has attributes that reference the corresponding gateway, assertion provider or authorization decision elements in the organisational unit “CHEndpoint”. Furthermore, each gateway element is prefixed with the shcIssuerName of its community.
     */
    private static boolean _validateSearchRequestSpec_Constraint_ch_cpi_006(net.ihe.gazelle.hpd.SearchRequest aClass) {
        return (!aClass.matches(((String) aClass.getDn()).toLowerCase(), "^uid=.+:.+") || (aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*ou=chcommunity,dc=cpi.*") && net.ihe.gazelle.gen.common.CommonOperationsStatic.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.25.1.15.7", ((String) aClass.getDn()).toLowerCase().substring((((String) aClass.getDn()).indexOf(":") + 1 + Integer.valueOf(1)) - 1, (((String) aClass.getDn()).indexOf(",") + 1 - Integer.valueOf(1))))));

    }

    /**
     * Validation of class-constraint : SearchRequestSpec
     * Verify if an element of type SearchRequestSpec can be validated by SearchRequestSpec
     */
    public static boolean _isSearchRequestSpec(net.ihe.gazelle.hpd.SearchRequest aClass) {
        return true;
    }

    /**
     * Validate as a template
     * name ::   SearchRequestSpec
     * class ::  net.ihe.gazelle.hpd.SearchRequest
     */
    public static void _validateSearchRequestSpec(net.ihe.gazelle.hpd.SearchRequest aClass, String location, List<Notification> diagnostic) {
        if (_isSearchRequestSpec(aClass)) {
            executeCons_SearchRequestSpec_Constraint_ch_cpi_002(aClass, location, diagnostic);
            executeCons_SearchRequestSpec_Constraint_ch_cpi_004(aClass, location, diagnostic);
            executeCons_SearchRequestSpec_Constraint_ch_cpi_006(aClass, location, diagnostic);
        }
    }

    private static void executeCons_SearchRequestSpec_Constraint_ch_cpi_002(net.ihe.gazelle.hpd.SearchRequest aClass,
                                                                            String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateSearchRequestSpec_Constraint_ch_cpi_002(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("constraint_ch_cpi_002");
        notif.setDescription("The searchRequest is supported on the CPI root or one of its child elements: ou=CHCommunity or ou=CHEndpoint.");
        notif.setLocation(location);
        notif.setIdentifiant("chcpi-SearchRequestSpec-constraint_ch_cpi_002");
        notif.getAssertions().add(new Assertion("CH-CPI", "CH-CPI-002"));
        diagnostic.add(notif);
    }

    private static void executeCons_SearchRequestSpec_Constraint_ch_cpi_004(net.ihe.gazelle.hpd.SearchRequest aClass,
                                                                            String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateSearchRequestSpec_Constraint_ch_cpi_004(aClass))) {
                notif = new Warning();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Warning();
        }
        notif.setTest("constraint_ch_cpi_004");
        notif.setDescription("The CPI base distinguished name / directory root is dc=CPI,o=BAG,c=CH.");
        notif.setLocation(location);
        notif.setIdentifiant("chcpi-SearchRequestSpec-constraint_ch_cpi_004");
        notif.getAssertions().add(new Assertion("CH-CPI", "CH-CPI-004"));
        diagnostic.add(notif);
    }

    private static void executeCons_SearchRequestSpec_Constraint_ch_cpi_006(net.ihe.gazelle.hpd.SearchRequest aClass,
                                                                            String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateSearchRequestSpec_Constraint_ch_cpi_006(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("constraint_ch_cpi_006");
        notif.setDescription("Each community of object class “CHCommunity” has attributes that reference the corresponding gateway, assertion provider or authorization decision elements in the organisational unit “CHEndpoint”. Furthermore, each gateway element is prefixed with the shcIssuerName of its community.");
        notif.setLocation(location);
        notif.setIdentifiant("chcpi-SearchRequestSpec-constraint_ch_cpi_006");
        notif.getAssertions().add(new Assertion("CH-CPI", "CH-CPI-006"));
        diagnostic.add(notif);
    }

}
