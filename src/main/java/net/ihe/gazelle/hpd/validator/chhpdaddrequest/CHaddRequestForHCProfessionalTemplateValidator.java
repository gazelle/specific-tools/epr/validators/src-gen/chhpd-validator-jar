package net.ihe.gazelle.hpd.validator.chhpdaddrequest;

import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


/**
 * class :        CHaddRequestForHCProfessionalTemplate
 * package :   chhpdAddRequest
 * Template Class
 * Template identifier :
 * Class of test : AddRequest
 */
public final class CHaddRequestForHCProfessionalTemplateValidator {


    private CHaddRequestForHCProfessionalTemplateValidator() {
    }


    /**
     * Validation of instance by a constraint : ch_hpd_009_LastName
     * Provider Last Name (sn) MUST be Single-valued
     */
    private static boolean _validateCHaddRequestForHCProfessionalTemplate_Ch_hpd_009_LastName(net.ihe.gazelle.hpd.AddRequest aClass) {
        java.util.ArrayList<DsmlAttr> result1;
        result1 = new java.util.ArrayList<DsmlAttr>();

        /* Iterator Select: Select all elements which fulfill the condition. */
        try {
            for (DsmlAttr anElement1 : aClass.getAttr()) {
                if ((anElement1.getName().toLowerCase().equals("sn") && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getValue())).equals(Integer.valueOf(1)))) {
                    result1.add(anElement1);
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(1));


    }

    /**
     * Validation of instance by a constraint : ch_hpd_014_Metadata_attributes
     * Value of attributes HCProfessional.HcProfession and HCProfessional.HcSpecialisation must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>]
     */
    private static boolean _validateCHaddRequestForHCProfessionalTemplate_Ch_hpd_014_Metadata_attributes(net.ihe.gazelle.hpd.AddRequest aClass) {
        java.util.ArrayList<DsmlAttr> result3;
        result3 = new java.util.ArrayList<DsmlAttr>();

        /* Iterator Select: Select all elements which fulfill the condition. */
        try {
            for (DsmlAttr anElement1 : aClass.getAttr()) {
                if (anElement1.getName().toLowerCase().equals("hcprofession")) {
                    result3.add(anElement1);
                }
                // no else
            }
        } catch (Exception e) {
        }
        java.util.ArrayList<String> result2;
        result2 = new java.util.ArrayList<String>();

        /* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
        try {
            for (DsmlAttr anElement2 : result3) {
                result2.addAll(anElement2.getValue());
            }
        } catch (Exception e) {
        }
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (String anElement3 : result2) {
                String hcprofession2;
                hcprofession2 = anElement3.substring(Integer.valueOf(5) - 1, anElement3.length());

                String hcprofession3;
                hcprofession3 = hcprofession2.substring((hcprofession2.indexOf(":") + 1 + Integer.valueOf(1)) - 1, hcprofession2.length());


                if (!(aClass.matches(anElement3, "^BAG:[^:]*:[^:]*(:[^:]*)?") && ((!aClass.matches(anElement3, "^BAG:[^:]*:[^:]*") || net.ihe.gazelle.gen.common.CommonOperationsStatic.matchesValueSet("2.16.756.5.30.1.127.3.10.8.1", hcprofession3, hcprofession2.substring(Integer.valueOf(1) - 1, (hcprofession2.indexOf(":") + 1 - Integer.valueOf(1))).toLowerCase(), null, null)) && (!aClass.matches(anElement3, "^BAG:[^:]*:[^:]*:[^:]*") || net.ihe.gazelle.gen.common.CommonOperationsStatic.matchesValueSet("2.16.756.5.30.1.127.3.10.8.1", hcprofession3.substring(Integer.valueOf(1) - 1, (hcprofession3.indexOf(":") + 1 - Integer.valueOf(1))), hcprofession2.substring(Integer.valueOf(1) - 1, (hcprofession2.indexOf(":") + 1 - Integer.valueOf(1))).toLowerCase(), null, null))))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        java.util.ArrayList<DsmlAttr> result6;
        result6 = new java.util.ArrayList<DsmlAttr>();

        /* Iterator Select: Select all elements which fulfill the condition. */
        try {
            for (DsmlAttr anElement4 : aClass.getAttr()) {
                if (anElement4.getName().toLowerCase().equals("hcspecialisation")) {
                    result6.add(anElement4);
                }
                // no else
            }
        } catch (Exception e) {
        }
        java.util.ArrayList<String> result5;
        result5 = new java.util.ArrayList<String>();

        /* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
        try {
            for (DsmlAttr anElement5 : result6) {
                result5.addAll(anElement5.getValue());
            }
        } catch (Exception e) {
        }
        Boolean result4;
        result4 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (String anElement6 : result5) {
                String hcprofession2;
                hcprofession2 = anElement6.substring(Integer.valueOf(5) - 1, anElement6.length());

                String hcprofession3;
                hcprofession3 = hcprofession2.substring((hcprofession2.indexOf(":") + 1 + Integer.valueOf(1)) - 1, hcprofession2.length());


                if (!(aClass.matches(anElement6, "^BAG:[^:]*:[^:]*(:[^:]*)?") && ((!aClass.matches(anElement6, "^BAG:[^:]*:[^:]*") || net.ihe.gazelle.gen.common.CommonOperationsStatic.matchesValueSet("2.16.756.5.30.1.127.3.10.8.2", hcprofession3, hcprofession2.substring(Integer.valueOf(1) - 1, (hcprofession2.indexOf(":") + 1 - Integer.valueOf(1))).toLowerCase(), null, null)) && (!aClass.matches(anElement6, "^BAG:[^:]*:[^:]*:[^:]*") || net.ihe.gazelle.gen.common.CommonOperationsStatic.matchesValueSet("2.16.756.5.30.1.127.3.10.8.2", hcprofession3.substring(Integer.valueOf(1) - 1, (hcprofession3.indexOf(":") + 1 - Integer.valueOf(1))), hcprofession2.substring(Integer.valueOf(1) - 1, (hcprofession2.indexOf(":") + 1 - Integer.valueOf(1))).toLowerCase(), null, null))))) {
                    result4 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return (result1 && result4);


    }

    /**
     * Validation of template-constraint by a constraint : CHaddRequestForHCProfessionalTemplate
     * Verify if an element can be token as a Template of type CHaddRequestForHCProfessionalTemplate
     */
    private static boolean _isCHaddRequestForHCProfessionalTemplateTemplate(net.ihe.gazelle.hpd.AddRequest aClass) {
        return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=hcprofessional){1}.*");

    }

    /**
     * Validation of class-constraint : CHaddRequestForHCProfessionalTemplate
     * Verify if an element of type CHaddRequestForHCProfessionalTemplate can be validated by CHaddRequestForHCProfessionalTemplate
     */
    public static boolean _isCHaddRequestForHCProfessionalTemplate(net.ihe.gazelle.hpd.AddRequest aClass) {
        return _isCHaddRequestForHCProfessionalTemplateTemplate(aClass);
    }

    /**
     * Validate as a template
     * name ::   CHaddRequestForHCProfessionalTemplate
     * class ::  net.ihe.gazelle.hpd.AddRequest
     */
    public static void _validateCHaddRequestForHCProfessionalTemplate(net.ihe.gazelle.hpd.AddRequest aClass, String location, List<Notification> diagnostic) {
        if (_isCHaddRequestForHCProfessionalTemplate(aClass)) {
            executeCons_CHaddRequestForHCProfessionalTemplate_Ch_hpd_009_LastName(aClass, location, diagnostic);
            executeCons_CHaddRequestForHCProfessionalTemplate_Ch_hpd_014_Metadata_attributes(aClass, location, diagnostic);
        }
    }

    private static void executeCons_CHaddRequestForHCProfessionalTemplate_Ch_hpd_009_LastName(net.ihe.gazelle.hpd.AddRequest aClass,
                                                                                              String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHaddRequestForHCProfessionalTemplate_Ch_hpd_009_LastName(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_hpd_009_LastName");
        notif.setDescription("Provider Last Name (sn) MUST be Single-valued");
        notif.setLocation(location);
        notif.setIdentifiant("chhpdAddRequest-CHaddRequestForHCProfessionalTemplate-ch_hpd_009_LastName");
        notif.getAssertions().add(new Assertion("CH-HPD", "CH-HPD-009"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHaddRequestForHCProfessionalTemplate_Ch_hpd_014_Metadata_attributes(net.ihe.gazelle.hpd.AddRequest aClass,
                                                                                                         String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHaddRequestForHCProfessionalTemplate_Ch_hpd_014_Metadata_attributes(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_hpd_014_Metadata_attributes");
        notif.setDescription("Value of attributes HCProfessional.HcProfession and HCProfessional.HcSpecialisation must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>]");
        notif.setLocation(location);
        notif.setIdentifiant("chhpdAddRequest-CHaddRequestForHCProfessionalTemplate-ch_hpd_014_Metadata_attributes");
        notif.getAssertions().add(new Assertion("CH-HPD", "CH-HPD-014"));
        diagnostic.add(notif);
    }

}
