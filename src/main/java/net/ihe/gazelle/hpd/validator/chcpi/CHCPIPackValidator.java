package net.ihe.gazelle.hpd.validator.chcpi;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


public class CHCPIPackValidator implements ConstraintValidatorModule {


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     */
    public CHCPIPackValidator() {
    }


    /**
     * Validation of instance of an object
     */
    public void validate(Object obj, String location, List<Notification> diagnostic) {

        if (obj instanceof net.ihe.gazelle.hpd.AttributeDescription) {
            net.ihe.gazelle.hpd.AttributeDescription aClass = (net.ihe.gazelle.hpd.AttributeDescription) obj;
            AttributeDescriptionSpecValidator._validateAttributeDescriptionSpec(aClass, location, diagnostic);
        }
        if (obj instanceof net.ihe.gazelle.hpd.AttributeValueAssertion) {
            net.ihe.gazelle.hpd.AttributeValueAssertion aClass = (net.ihe.gazelle.hpd.AttributeValueAssertion) obj;
            AttributeValueAssertionSpecValidator._validateAttributeValueAssertionSpec(aClass, location, diagnostic);
        }
        if (obj instanceof net.ihe.gazelle.hpd.BatchRequest) {
            net.ihe.gazelle.hpd.BatchRequest aClass = (net.ihe.gazelle.hpd.BatchRequest) obj;
            BatchRequestSpecValidator._validateBatchRequestSpec(aClass, location, diagnostic);
        }
        if (obj instanceof net.ihe.gazelle.hpd.MatchingRuleAssertion) {
            net.ihe.gazelle.hpd.MatchingRuleAssertion aClass = (net.ihe.gazelle.hpd.MatchingRuleAssertion) obj;
            MatchingRuleAssertionSpecValidator._validateMatchingRuleAssertionSpec(aClass, location, diagnostic);
        }
        if (obj instanceof net.ihe.gazelle.hpd.SearchRequest) {
            net.ihe.gazelle.hpd.SearchRequest aClass = (net.ihe.gazelle.hpd.SearchRequest) obj;
            SearchRequestSpecValidator._validateSearchRequestSpec(aClass, location, diagnostic);
        }
        if (obj instanceof net.ihe.gazelle.hpd.SubstringFilter) {
            net.ihe.gazelle.hpd.SubstringFilter aClass = (net.ihe.gazelle.hpd.SubstringFilter) obj;
            SubstringFilterSpecValidator._validateSubstringFilterSpec(aClass, location, diagnostic);
        }

    }

}

