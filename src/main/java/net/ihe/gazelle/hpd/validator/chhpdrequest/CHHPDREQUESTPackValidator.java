package net.ihe.gazelle.hpd.validator.chhpdrequest;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


public class CHHPDREQUESTPackValidator implements ConstraintValidatorModule {


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     */
    public CHHPDREQUESTPackValidator() {
    }


    /**
     * Validation of instance of an object
     */
    public void validate(Object obj, String location, List<Notification> diagnostic) {


    }

}

