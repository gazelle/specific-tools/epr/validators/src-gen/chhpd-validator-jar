package net.ihe.gazelle.hpd.validator.chhpd;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


public class CHHPDPackValidator implements ConstraintValidatorModule {


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     */
    public CHHPDPackValidator() {
    }


    /**
     * Validation of instance of an object
     */
    public void validate(Object obj, String location, List<Notification> diagnostic) {


    }

}

