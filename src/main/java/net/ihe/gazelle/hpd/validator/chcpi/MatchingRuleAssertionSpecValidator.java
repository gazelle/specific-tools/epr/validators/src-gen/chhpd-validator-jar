package net.ihe.gazelle.hpd.validator.chcpi;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


/**
 * class :        MatchingRuleAssertionSpec
 * package :   chcpi
 * Constraint Spec Class
 * class of test : MatchingRuleAssertion
 */
public final class MatchingRuleAssertionSpecValidator {


    private MatchingRuleAssertionSpecValidator() {
    }


    /**
     * Validation of instance by a constraint : constraint_ch_cpi_attr_mathingrule
     * The defined attributes for the CHCommunity and CHEndpoint object classes are listed in Appendix B. LDAP Schema Overview
     */
    private static boolean _validateMatchingRuleAssertionSpec_Constraint_ch_cpi_attr_mathingrule(net.ihe.gazelle.hpd.MatchingRuleAssertion aClass) {
        return (!(((String) aClass.getName()) == null) && (aClass.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.25.1.15.2", aClass.getName().toLowerCase()) || aClass.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.25.1.15.3", aClass.getName().toLowerCase())));

    }

    /**
     * Validation of class-constraint : MatchingRuleAssertionSpec
     * Verify if an element of type MatchingRuleAssertionSpec can be validated by MatchingRuleAssertionSpec
     */
    public static boolean _isMatchingRuleAssertionSpec(net.ihe.gazelle.hpd.MatchingRuleAssertion aClass) {
        return true;
    }

    /**
     * Validate as a template
     * name ::   MatchingRuleAssertionSpec
     * class ::  net.ihe.gazelle.hpd.MatchingRuleAssertion
     */
    public static void _validateMatchingRuleAssertionSpec(net.ihe.gazelle.hpd.MatchingRuleAssertion aClass, String location, List<Notification> diagnostic) {
        if (_isMatchingRuleAssertionSpec(aClass)) {
            executeCons_MatchingRuleAssertionSpec_Constraint_ch_cpi_attr_mathingrule(aClass, location, diagnostic);
        }
    }

    private static void executeCons_MatchingRuleAssertionSpec_Constraint_ch_cpi_attr_mathingrule(net.ihe.gazelle.hpd.MatchingRuleAssertion aClass,
                                                                                                 String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateMatchingRuleAssertionSpec_Constraint_ch_cpi_attr_mathingrule(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("constraint_ch_cpi_attr_mathingrule");
        notif.setDescription("The defined attributes for the CHCommunity and CHEndpoint object classes are listed in Appendix B. LDAP Schema Overview");
        notif.setLocation(location);
        notif.setIdentifiant("chcpi-MatchingRuleAssertionSpec-constraint_ch_cpi_attr_mathingrule");
        notif.getAssertions().add(new Assertion("CH-CPI", "CH-CPI-003"));
        notif.getAssertions().add(new Assertion("CH-CPI", "CH-CPI-005"));
        diagnostic.add(notif);
    }

}
