package net.ihe.gazelle.validator.test.chcpi;

import org.junit.Assert;
import org.junit.Test;

public class SubstringFilterSpecTest {

    CPITestUtil testExecutor = new CPITestUtil();

    @Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. KO.
    public void test_ko_constraint_cpi_SubstringFilter() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.SubstringFilter.KO.xml",
                        "chcpi-SubstringFilterSpec-constraint_ch_cpi_attr_substr"));
    }

    @Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. OK.
    public void test_ok_constraint_cpi_SubstringFilter() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.OK.xml",
                        "chcpi-SubstringFilterSpec-constraint_ch_cpi_attr_substr"));
    }
}		
