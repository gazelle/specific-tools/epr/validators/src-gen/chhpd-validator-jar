package net.ihe.gazelle.validator.test.chhpd;

import org.junit.Assert;
import org.junit.Test;
public class CHaddRequestForHCProfessionalTemplateTest {

	AddRequestTestUtil testExecutor = new AddRequestTestUtil();

	@Test
	// Provider Last Name (sn) MUST be Single-valued. KO.
	public void test_ko_constraint_hpdaddrequest_LastName() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/ehealthsuisse/HPDIndividual/ProviderLastName.KO.xml",
						"chhpdAddRequest-CHaddRequestForHCProfessionalTemplate-ch_hpd_009_LastName"));
	}

	@Test
	// Provider Last Name (sn) MUST be Single-valued. OK.
	public void test_ok_constraint_hpdaddrequest_LastName() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/ehealthsuisse/HPDIndividual/Provider.OK.xml",
						"chhpdAddRequest-CHaddRequestForHCProfessionalTemplate-ch_hpd_009_LastName"));
	}

	@Test
	// Value of attributes HCProfessional.HcProfession and HCProfessional.HcSpecialisation must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>] KO.
	public void test_ko_constraint_hpdaddrequest_Metadata() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/ehealthsuisse/HPDIndividual/Provider.Metadata.KO.xml",
						"chhpdAddRequest-CHaddRequestForHCProfessionalTemplate-ch_hpd_014_Metadata_attributes"));
	}

	@Test
	// Value of attributes HCProfessional.HcProfession and HCProfessional.HcSpecialisation must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>] KO.
	public void test_ko_constraint_hpdaddrequest_Metadata2() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/ehealthsuisse/HPDIndividual/Provider.Metadata2.KO.xml",
						"chhpdAddRequest-CHaddRequestForHCProfessionalTemplate-ch_hpd_014_Metadata_attributes"));
	}

	@Test
	// Value of attributes HCProfessional.HcProfession and HCProfessional.HcSpecialisation must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>] OK.
	public void test_ok_constraint_hpdaddrequest_Metadata2() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/ehealthsuisse/HPDIndividual/Provider.Metadata.OK.xml",
						"chhpdAddRequest-CHaddRequestForHCProfessionalTemplate-ch_hpd_014_Metadata_attributes"));
	}

	@Test
	// Value of attributes HCProfessional.HcProfession and HCProfessional.HcSpecialisation must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>]. OK.
	public void test_ok_constraint_hpdaddrequest_Metadata() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/ehealthsuisse/HPDIndividual/Provider.OK.xml",
						"chhpdAddRequest-CHaddRequestForHCProfessionalTemplate-ch_hpd_014_Metadata_attributes"));
	}
}		
