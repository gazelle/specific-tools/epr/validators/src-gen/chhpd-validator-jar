package net.ihe.gazelle.validator.test.chcpi;

import org.junit.Assert;
import org.junit.Test;

public class MatchingRuleAssertionSpecTest {

    CPITestUtil testExecutor = new CPITestUtil();

    @Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. KO.
    public void test_ko_constraint_cpi_matchingRuleAssertion() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.MatchingRuleAssertion.KO.xml",
                        "chcpi-MatchingRuleAssertionSpec-constraint_ch_cpi_attr_mathingrule"));
    }

    @Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. OK.
    public void test_ok_constraint_cpi_attributeValueAssertion() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.OK.xml",
                        "chcpi-MatchingRuleAssertionSpec-constraint_ch_cpi_attr_mathingrule"));
    }
}		
